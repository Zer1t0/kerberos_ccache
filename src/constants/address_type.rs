//! Types of addresses used by Kerberos protocol.
//!
//! # References
//! * RFC 4120, Section 7.5.3.

pub const NETBIOS: i32 = 20;
